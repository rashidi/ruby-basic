class Name
  attr_reader :first, :last

  def first=(first)
    if first == nil or first.size == 0
      raise ArgumentError.new('Everyone must have a first name.')
    end
    first = first.dup
    first[0] = first[0].chr.capitalize
    @first = first
  end

  def last=(last)
    if last == nil or last.size == 0
      raise ArgumentError.new('Everyone must have a last name.')
    end
    @last = last
  end

  def full_name
    "#{@first} #{@last}"
  end

  def initialize(first, last)
    self.first = first
    self.last = last
  end
end

johnson = Name.new('Johnson', 'Smith')

puts johnson.first
puts johnson.last
puts johnson.full_name;

johnson.first = nil
