=begin
  Ruby Numbers
  Usual operators:
    + addition
    - subtraction
    * multiplication
    / division
=end

puts 1 + 2
puts 2 * 3

# Integer division
# When you do arithmetic with integers, you'll get integer answers
puts 3 / 2
puts 10 - 11
puts 1.5 / 2.6
puts 5 % 3

# In Ruby modulo works as x - y * (x / y).floor
puts -7 % 3

# Both or and || return their first argument unless it is false, in which case they evaluate and return their second argument
# || has a higher precedence than or
puts nil || 2008
puts false || 2008
puts "ruby" or 2008



