=begin
  Ruby Strings
  In Ruby, strings are mutable
=end

# Can use " or ' for Strings, but ' is more efficient
puts "Hello Double"
puts 'Hello Single'

# Displaying a String n times
puts 'Hello ' * 3

# Defining a constant
PI = 3.1416
puts PI