hello = 'Hello'
year = 2012
world = 'World'

puts hello + ' ' + year.to_s + ' ' + world

x = '3'
y = 2

puts x.to_i * 2

# Appending to a String
a = 'Hello'
puts a<<' World.'

a = <<END_STR
This is the string
And a second line
END_STR

puts a