# Local variables
count = 0
_count = 0
cou_nt = 0

# Instance variables - Declared within an object always belongs to whatever object self refers to
@count = 0

# Class variables - Declared within a class
@@count = 0

# Global variables
$count = 0
$COUNTER = 0