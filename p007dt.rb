# Ruby is dynamic
x = 7       # integer
x = "house" # string
x = 7.5     # real

puts 'I love Ruby'.length

5.times { puts "Running five times!!\n" }

puts self
puts self.class