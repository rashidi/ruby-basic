# Methods that act as queries are often named with a trailing ?
# Methods that are "dangerous" or modify the receiver, might be named with a trailing ! (Bang methods)

# A simple method
def hello
  'Hello'
end

puts hello

# Method with an argument
def shout(name)
  'HELLO ' + name.upcase
end

puts shout('Godzilla')

def location(country="Malaysia", state="Selangor", city="Ampang")
  "#{city}, #{state}, #{country}"
end

puts location
puts location("Singapore")

puts "100 * 5 = #{100 * 5}"

# Alias
alias scream shout

puts scream("anaconda")

# Variable number of parameters
def many(*my_string)
  my_string.inspect
end

puts many('if', 'then', 'else')

def say_good_day(name)
  result = "Good day, #{name}"
  return result
end

puts say_good_day('Johnson')