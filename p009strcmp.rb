
s1 = 'St Peter'
s2 = 'St Peter'
s3 = 'Galileo'

if s1 == s2
  puts 'Identical content'
else
  puts 'Not identical content'
end

if (s1.eql?(s3))
  puts 'Identical content'
else
  puts 'Not identical content'
end

nottingham = [ 'robin hood', 'maid marian', 'little john' ]
puts nottingham[0]
puts nottingham[1]

gotham = %w{ bruce alfred dick }
puts gotham[0]
puts gotham[2]