unless 2 > 1
  puts "One is never bigger than two!" # this will not be printed unless the condition being changed
  exit
end

var = 0
while var < 5
  puts var
  var += 1
end

age = 17
puts (13...17).include?(age) ? "High school student" : "Not a high school student"

year = 2012
end_of_world = case
                 when year == 2012 then true
                 else false
               end

puts end_of_world