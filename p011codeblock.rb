def call_block
  puts 'Start of method'
  yield
  yield
  puts 'End of method'
end

call_block {puts 'In the block'}

def call_block_param
  yield('hello', 99)
end

call_block_param { | str, num| puts str + ' ' + num.to_s }