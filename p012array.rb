names = [ 'Anthony Edward Stark', 'Thor Odinson', 'Bruce Banner', 'Steve Rogers', 'Natasha Romanoff', 'Clinton Francis Barton' ]
names = names.sort # as the method suggested. Sorting the array

puts 'The Avengers True Identity'
puts "There are #{names.length} individuals. \nBegins with #{names.first} and ends with #{names.last}"

puts "\n"

names.each do |name|
  puts "Who is #{name}?"
end