a, b = 1 , 2, 3, 4

puts a # 1
puts b # 2

# Print environment variables
#ENV.each { |k, v| puts "#{k}: #{v}"}

# Casting
str = 'Hello'
print Array(str).class

str = "Hello\nWorld"
print Array(str)

a = [1, 2, 3, 4]
print a.class.ancestors