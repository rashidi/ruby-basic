=begin
def raise_exception
  puts 'I am before the raise'
  raise 'An error has occurred'
  puts 'I am after the raise'
end
raise_exception
=end

=begin
def inverse(x)
  raise ArgumentError, 'Argument is not numeric' unless x.is_a? Numeric
  1.0 / x
end
puts inverse(2)
puts inverse("not a  number")
=end

=begin
def raise_and_rescue
  begin
    puts 'I am before the raise'
    raise 'An error has occurred'
    puts 'I am after the raise'
  rescue
    puts 'I am rescued'
  end
  puts 'I am after the begin block.'
end
raise_and_rescue
=end

begin
  raise 'A test exception.'
rescue Exception => e
  puts e.message
  puts e.backtrace.inspect
end