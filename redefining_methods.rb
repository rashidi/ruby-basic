class RedefiningMethods
  def mtd
    puts "First definition of the method mtd"
  end

  def mtd
    puts "Second definition of method mtd"
  end
end

RedefiningMethods.new.mtd